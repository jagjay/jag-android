package com.jagformen.app;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.jagformen.app.com.jagformen.models.User;
import com.jagformen.app.com.jagformen.retrofit.JAGApi;
import com.jakewharton.rxbinding.view.RxView;
import com.jakewharton.rxbinding.widget.RxTextView;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;
import rx.Observable;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity  {

	private static String TAG = "LoginActivity";

	@Bind(R.id.et_email)
	EditText et_email;

	@Bind(R.id.et_password)
	EditText et_password;

	@Bind(R.id.pb_login)
	View pb_login;

	@Bind(R.id.til_email)
	TextInputLayout til_email;
	@Bind(R.id.til_password)
	TextInputLayout til_password;

	@Bind(R.id.b_sign_in)
	Button b_sign_in;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);

		ButterKnife.bind(this);


//		til_email.setErrorEnabled(true);
//		til_password.setErrorEnabled(true);
//
//		til_email.setError("Invalid email");
//		til_password.setError("Invalid Password");
//

		//et_email.setError("Email error");
		Observable<Boolean> emailObservable = RxTextView.textChanges(et_email)
				.map(inputText -> inputText.toString().length() > 0)
				.distinctUntilChanged();

		Observable<Boolean> passwordObservable = RxTextView.textChanges(et_password)
				.map(inputText -> inputText.toString().length() > 0);

		emailObservable.subscribe(isValid -> et_email.setError("!isValid"));
		passwordObservable.subscribe(isValid -> til_password.setErrorEnabled(!isValid));

		Observable.combineLatest(
				emailObservable,
				passwordObservable,
				(emailValid, passwordValid) -> emailValid && passwordValid)
				.distinctUntilChanged()
				.subscribe(valid -> b_sign_in.setEnabled(valid));


		RxView.clicks(b_sign_in).subscribe(view -> {
			// Replace below with your click handling code
			Log.d(TAG, "Clicked");


			User user = User.createOne();
			Call<User> call = JAGApi.getInstance().createUser(user);
			call.enqueue(new Callback<User>() {
				@Override
				public void onResponse(Response<User> response, Retrofit retrofit) {
					Log.d(TAG + ":onResponse()", "response=" + response);
				}

				@Override
				public void onFailure(Throwable t) {
					t.printStackTrace();
				}
			});

			Observable<User> fi = JAGApi.getInstance().createUserRx(user);

			Intent ii = new Intent(LoginActivity.this, RoleActivity.class);
			startActivity(ii);
		});


	}

}

