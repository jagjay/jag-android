package com.jagformen.app.com.jagformen.retrofit;

import com.jagformen.app.com.jagformen.models.User;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.logging.HttpLoggingInterceptor;

import retrofit.Call;
import retrofit.GsonConverterFactory;
import retrofit.Retrofit;
import retrofit.RxJavaCallAdapterFactory;
import retrofit.http.Body;
import retrofit.http.POST;
import rx.Observable;

/**
 * Created by jayant on 2/28/16.
 */
public class JAGApi {

	private static JAGApi instance;

	private JAGApi() {}

	public static JAGApi getInstance() {
		if (instance == null) {
			instance = new JAGApi();
			instance.client = createService(APIService.class);
		}
		return instance;
	}

	APIService client;

	public Call<User> createUser(User user) {
		return client.createUser(user);
	}
	public Observable<User> createUserRx(User user) {
		return client.createUserRx(user);
	}

	/*
	public Call<Forecast> getForecast(float lat, float lon) {
		return client.getForecast(lat, lon, "imperial", API_KEY);
	}

	public Observable<Forecast> getForecastRx(float lat, float lon) {
		return client.getForecastRx(lat, lon, "imperial", API_KEY);
	}


	public Call<Forecast> getForecast(String city) {
		return client.getForecast(city, "imperial", API_KEY);
	}

	private static String API_KEY = "7388bee4f61fc0487ee598005784777c";
    */

	public interface APIService {

		// NEVER call these DIRECTLY, use the .instance().methods() instead

		@POST("/profiles/api/customer/register/")
		Call<User> createUser(@Body User user);

		@POST("/profiles/api/customer/register/")
		Observable<User> createUserRx(@Body User user);

		/*
        @GET("/data/2.5/weather")
        Call<Forecast> getForecast(@Query("lat") float lat, @Query("lon") float lon, @Query("units") String units, @Query("APPID") String appId);

		@GET("/data/2.5/weather")
		Observable<Forecast> getForecastRx(@Query("lat") float lat, @Query("lon") float lon, @Query("units") String units, @Query("APPID") String appId);

        @GET("/data/2.5/weather")
        Call<Forecast> getForecast(@Query("q") String city, @Query("units") String units, @Query("APPID") String appId);
        */

    }

	static String API_BASE_URL = "https://jagbeautyandblades.com/";

    private static OkHttpClient httpClient = new OkHttpClient();

    private static Retrofit.Builder builder =
            new Retrofit.Builder()
					.baseUrl(API_BASE_URL)
					.addConverterFactory(GsonConverterFactory.create())
					.addCallAdapterFactory(RxJavaCallAdapterFactory.create());



    private static <S> S createService(Class<S> serviceClass) {

		HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
		//logging.setLevel(HttpLoggingInterceptor.Level.NONE);
		logging.setLevel(HttpLoggingInterceptor.Level.BODY);

		httpClient.interceptors().add(logging);

        Retrofit retrofit = builder.client(httpClient).build();

        return retrofit.create(serviceClass);
    }
}
