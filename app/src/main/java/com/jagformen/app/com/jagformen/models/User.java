package com.jagformen.app.com.jagformen.models;

public class User {

	private String email;
	private String password;
	private String first_name;
	private String last_name;
	private String phone;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFirstName() {
		return first_name;
	}

	public void setFirstName(String firstName) {
		this.first_name = firstName;
	}

	public String getLastName() {
		return last_name;
	}

	public void setLastName(String lastName) {
		this.last_name = lastName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public static User createOne() {

		User user = new User();
		user.setEmail("fake@fake.com");
		user.first_name = "FakeFirst";
		user.last_name = "FakeLast";
		user.phone = "0000000000";
		user.password = "fakepass";

		return user;


	}
}