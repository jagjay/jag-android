package com.jagformen.app;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LicensesActivity extends AppCompatActivity {

	private static String TAG = "LicensesActivity";


	@Bind(R.id.rv_licences)
	RecyclerView rv_licences;

	@Bind(R.id.btnShow)
	Button btnShow;

	@OnClick(R.id.btnShow)
	protected void btnShow() {
		Log.i(TAG + ":btnShow", "clicked btnShow");
		startActivity(new Intent(this, SignupActivity.class));
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_licenses);

		ButterKnife.bind(this);

		List<Student> list = new ArrayList<>();
		list.add(new Student("One"));
		list.add(new Student("Two"));
		list.add(new Student("Three"));

		// use this setting to improve performance if you know that changes
		// in content do not change the layout size of the RecyclerView
		rv_licences.setHasFixedSize(true);

		// use a linear layout manager
		rv_licences.setLayoutManager(new LinearLayoutManager(this));
		// create an Object for Adapter
		CardViewDataAdapter mAdapter = new CardViewDataAdapter(list);

		// set the adapter object to the Recyclerview
		rv_licences.setAdapter(mAdapter);

	}

	class Student {
		String name;

		public Student(String name) {
			this.name = name;
		}

		public boolean isChecked() {
			return isChecked;
		}

		public void setChecked(boolean checked) {
			isChecked = checked;
		}

		boolean isChecked;
	}

	public class CardViewDataAdapter extends
			RecyclerView.Adapter<CardViewDataAdapter.ViewHolder> {

		private List<Student> stList;


		public CardViewDataAdapter(List<Student> students) {
			this.stList = students;

		}

		// Create new views
		@Override
		public CardViewDataAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
																 int viewType) {
			// create a new view
			View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(
					R.layout.cardview_row, parent, false);

			// create ViewHolder

			ViewHolder viewHolder = new ViewHolder(itemLayoutView);

			return viewHolder;
		}

		@Override
		public void onBindViewHolder(ViewHolder viewHolder, int position) {

			final int pos = position;

			viewHolder.tvName.setText(stList.get(position).name);

			viewHolder.tvEmailId.setText(stList.get(position).name);

			viewHolder.chkSelected.setChecked(stList.get(position).isChecked());

			viewHolder.chkSelected.setTag(stList.get(position));


			viewHolder.chkSelected.setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) {
					CheckBox cb = (CheckBox) v;
					Student contact = (Student) cb.getTag();

					contact.setChecked(cb.isChecked());
					stList.get(pos).setChecked(cb.isChecked());

					Toast.makeText(
							v.getContext(),
							"Clicked on Checkbox: " + cb.getText() + " is "
									+ cb.isChecked(), Toast.LENGTH_LONG).show();
				}
			});

		}

		// Return the size arraylist
		@Override
		public int getItemCount() {
			return stList.size();
		}

		public  class ViewHolder extends RecyclerView.ViewHolder {

			public TextView tvName;
			public TextView tvEmailId;

			public CheckBox chkSelected;


			public ViewHolder(View itemLayoutView) {
				super(itemLayoutView);

				tvName = (TextView) itemLayoutView.findViewById(R.id.tvName);

				tvEmailId = (TextView) itemLayoutView.findViewById(R.id.tvEmailId);
				chkSelected = (CheckBox) itemLayoutView
						.findViewById(R.id.chkSelected);

			}

		}



	}



}


