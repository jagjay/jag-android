package com.jagformen.app;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.DatePicker;
import android.widget.TimePicker;

import butterknife.Bind;
import butterknife.ButterKnife;

public class ScheduleActivity extends AppCompatActivity {

	@Bind(R.id.pickerdate)
	DatePicker pickerdate;

	@Bind(R.id.pickertime)
	TimePicker pickertime;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_schedule);

		ButterKnife.bind(this);


	}
}
