package com.jagformen.app;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

public class SignupActivity extends AppCompatActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_signup);

// Begin the transaction
		FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
// Replace the contents of the container with the new fragment
		ft.replace(R.id.fragment, Frag1.newInstance("dd", "dd"));
// or ft.add(R.id.your_placeholder, new FooFragment());
// Complete the changes added above
		ft.commit();

	}
}
