package com.jagformen.app;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Button;

import com.jakewharton.rxbinding.view.RxView;

import butterknife.Bind;
import butterknife.ButterKnife;

public class RoleActivity extends AppCompatActivity {

	private static String TAG = "RoleActivity";

	@Bind(R.id.b_role_customer)
	Button b_role_customer;

	@Bind(R.id.b_role_provider)
	Button b_role_provider;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_role);

		ButterKnife.bind(this);

		RxView.clicks(b_role_customer).subscribe(view -> {
			// Replace below with your click handling code
			Log.d(TAG, "Clicked");
			Intent ii = new Intent(RoleActivity.this, ScheduleActivity.class);
			startActivity(ii);
		});

		RxView.clicks(b_role_provider).subscribe(view -> {
			// Replace below with your click handling code
  			Log.d(TAG, "Clicked");
			Intent ii = new Intent(RoleActivity.this, LicensesActivity.class);
			startActivity(ii);
		});


	}
}
