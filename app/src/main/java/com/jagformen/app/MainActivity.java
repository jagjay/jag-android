package com.jagformen.app;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.jakewharton.rxbinding.view.RxView;
import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

	private static String TAG = "MainActivity";
	static String EXTRA_MESSAGE = "eee";

	@Bind(R.id.b_signup)
	Button b_signup;

	@Bind(R.id.vp)
	ViewPager vp;

	@Bind(R.id.titles)
	CirclePageIndicator circlePageIndicator;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		ButterKnife.bind(this);

		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);

		List<Fragment> fList = new ArrayList<>();
		fList.add(MyFragment.newInstance("Fragment 1"));
		fList.add(MyFragment.newInstance("Fragment 2"));
		fList.add(MyFragment.newInstance("Fragment 3"));

		MyPageAdapter adapter = new MyPageAdapter(getSupportFragmentManager(), fList);
		vp.setAdapter(adapter);
		circlePageIndicator.setViewPager(vp);

		RxView.clicks(b_signup).subscribe(view -> {
			// Replace below with your click handling code
			Log.d(TAG, "Clicked");

			Intent ii = new Intent(MainActivity.this, LoginActivity.class);
			startActivity(ii);

		});
	}

	class MyPageAdapter extends FragmentPagerAdapter {
		private List<Fragment> fragments;
		public MyPageAdapter(FragmentManager fm, List<Fragment> fragments) {
			super(fm);
			this.fragments = fragments;
		}
		@Override
		public Fragment getItem(int position) {
			return this.fragments.get(position);
		}
		@Override
		public int getCount() {
			return this.fragments.size();
		}
	}

	public static class MyFragment extends Fragment {
     public static final MyFragment newInstance(String message)
     {
       MyFragment f = new MyFragment();
       Bundle bdl = new Bundle(1);
       bdl.putString(EXTRA_MESSAGE, message);
       f.setArguments(bdl);
       return f;
     }
     @Override
     public View onCreateView(LayoutInflater inflater, ViewGroup container,
							  Bundle savedInstanceState) {
       String message = getArguments().getString(EXTRA_MESSAGE);
       View v = inflater.inflate(R.layout.frag1, container, false);
       TextView messageTextView = (TextView)v.findViewById(R.id.textView);
       messageTextView.setText(message);
       return v;
	 }
    }
}
